package gympalm.br.network

import gympalm.br.Models.CardapioModel
import gympalm.br.Models.Estabelecimentos
import gympalm.br.Models.EstabelecimentosDetalhe
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface NetworkApi {
    @GET("getCardapio")
    fun getCardapio(
            @Query("idEstabelecimento") idEstabelecimento : String
    ) : Observable<List<CardapioModel>>

    @GET("webservice/getListagem.php?")
    fun getEstabelecimentos(
            @Query("lat") idEstado : String,
            @Query("long") idCidade : String
    ) : Observable<List<Estabelecimentos>>

    @GET("webservice/getDetalhes.php?")
    fun getDetalhe(
            @Query("token") token : String
    ) : Observable<EstabelecimentosDetalhe>
}