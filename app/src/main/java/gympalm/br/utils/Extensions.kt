package gympalm.br.utils

import android.content.Context
import android.content.res.Resources
import android.support.v7.app.ActionBar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import gympalm.br.R


fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}


fun ImageView.loadUrl(url: String) {
    Picasso.get().load(url).into(this)
}

fun ActionBar.changeActionBar(context: Context) {
    var backArrow = context.resources.getDrawable(R.mipmap.ic_keyboard_arrow_left_black)
    this.setHomeAsUpIndicator(backArrow)
}

fun ImageView.visible() {
    this.visibility = View.VISIBLE
}

fun ImageView.gone() {
    this.visibility = View.GONE
}

fun TextView.visible() {
    this.visibility = View.VISIBLE
}

fun TextView.gone() {
    this.visibility = View.GONE
}