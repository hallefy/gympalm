package gympalm.br.utils

import gympalm.br.Models.CardapioModel
import gympalm.br.Models.ComentariosModel
import gympalm.br.Models.Estabelecimentos

class RandomData{

    fun comentarios() : List<ComentariosModel> {
        return listOf(
                ComentariosModel(
                        "This is an example of how to write WSGI middleware with WebOb. The specific example adds a simple comment form to HTML web pages; ",
                        "Há 30min",
                        "Hallefy"
                ),
                ComentariosModel(
                        "It’s a sandwich and it’s dry as hell. The burger has no flavor, " +
                                "barely any sauce on it and they forgot the onions on mine. Bread is also overwhelming (dryness). " +
                                "Took one bite and couldn’t finish it. Waste of " +
                                "\$ and guarantee it won’t stay on the menu long. ",
                        "Há 12h",
                        "Fernanda"
                ),
                ComentariosModel(
                        "IMan, are you gonna bring back the white/swiss cheese that " +
                                "was on the Sourdough Bacon Cheeseburger back in the early 2000s? ",
                        "Há 1 semana",
                        "Otavio"
                ),
                ComentariosModel(
                        "All in favor of it being called a Burger-Sandwich just to watch the" +
                                "fuckery in ensues. Like, Comment, Both.",
                        "Há 1 mês",
                        "Rafael"
                )
        )
    }

    fun cardapio() : List<CardapioModel> {
        return listOf(
                CardapioModel("PICANHA CHEDDAR E BACON",
                        "Pão polvilhado com farinha de milho, um suculento " +
                                "hambúrguer de picanha, cheddar cremoso, " +
                                "maionese e pra ficar ainda mais saboroso, fatias de bacon ",
                        "R$ 24,00"),
                CardapioModel("MEGA STACKER ATÔMICO 2.0",
                        "Mega Stacker 2.0 que você já conhece, agora com queijo " +
                                "e bacon nas duas camadas. ",
                        "R$ 30,00"),
                CardapioModel("WHOPPER DUPLO",
                        "Pão com gergelim, dois suculentos hambúrgueres de pura carne bovina, " +
                                "duas fatias de queijo derretido, quatro fatias de picles, alface, tomate, cebola, " +
                                "maionese e ketchup.",
                        "R$ 20,00"),
                CardapioModel("KING DUPLO",
                        "Pão com gergelim, dois suculentos hambúrgueres de pura carne bovina, " +
                                "duas fatias de queijo derretido, duas fatias de picles, ketchup e mostarda.",
                        "R$ 28,00"),
                CardapioModel("HAMBÚRGUER",
                        "Pão com gergelim, um saboroso hambúrguer de pura carne bovina, duas fatias de picles, ketchup e mostarda. ",
                        "R$ 12,00"),
                CardapioModel("STACKER TRIPLO",
                        "Carne, queijo e 2 fatias de bacon. Tudo multiplicado por 3, acompanhado do saboroso molho Stacker em um pão macio com gergelim.",
                        "R$ 35,00")
        )
    }
}