package gympalm.br.utils

import android.content.Context
import android.content.SharedPreferences
import java.security.AccessControlContext
import android.app.Activity


class Storage{
    private var mSharedPref: SharedPreferences? = null

    fun init(context : Context){
        if (mSharedPref == null)
            mSharedPref = context.getSharedPreferences(KEY.KEY_SHARED_PREFERENCES, Activity.MODE_PRIVATE)
    }

    fun read(key: String): String? {
        return mSharedPref!!.getString(key, "")
    }

    fun write(key: String, value: String) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putString(key, value)
        prefsEditor.apply()
        prefsEditor.commit()
    }

    fun read(key: String, defValue: Boolean): Boolean {
        return mSharedPref!!.getBoolean(key, defValue)
    }

    fun write(key: String, value: Boolean) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putBoolean(key, value)
        prefsEditor.apply()
        prefsEditor.commit()
    }

    fun write(key: String, value: Float) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putFloat(key, value)
        prefsEditor.apply()
        prefsEditor.commit()
    }

    fun readFloat(key: String): Float {
        return mSharedPref!!.getFloat(key, 0.toFloat())
    }

    fun read(key: String, defValue: Int): Int {
        return mSharedPref!!.getInt(key, defValue)
    }

    fun write(key: String, value: Int) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putInt(key, value)
        prefsEditor.apply()
        prefsEditor.commit()
    }
}