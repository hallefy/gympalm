package gympalm.br.utils

import android.app.AlertDialog
import android.content.Context
import android.util.Log

class ShowAlertMessageSimple(var context: Context){

    fun showAlert(title : String, message : String, buttonPositive : String){
        AlertDialog.Builder(context).apply {
            setTitle(title)
            setMessage(message)
            setPositiveButton(buttonPositive, { _, _ ->
            })
            show()
        }
    }
}