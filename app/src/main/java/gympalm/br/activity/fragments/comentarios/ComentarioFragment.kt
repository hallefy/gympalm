package gympalm.br.activity.fragments.comentarios

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import gympalm.br.R
import gympalm.br.activity.fragments.comentarios.adapter.ComentariosAdapter
import gympalm.br.utils.RandomData
import kotlinx.android.synthetic.main.fragment_comentario.*

class ComentarioFragment : Fragment() {
    lateinit var token : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val bundle = this.arguments
        if (bundle != null) {
            token = bundle.getString("token", "")
            Log.i("hallefy: ","token:$token")
        }

        return inflater.inflate(R.layout.fragment_comentario, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler_comentarios.layoutManager = LinearLayoutManager(this.context)
        recycler_comentarios.adapter = ComentariosAdapter(this.context!!, RandomData().comentarios())
    }
}
