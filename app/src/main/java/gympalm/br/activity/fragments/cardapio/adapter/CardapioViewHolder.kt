package gympalm.br.activity.fragments.cardapio.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import gympalm.br.Models.CardapioModel
import kotlinx.android.synthetic.main.card_cardapio.view.*

class CardapioViewHolder(val context: Context, itemView : View) : RecyclerView.ViewHolder(itemView) {

    fun bind(cardapio : CardapioModel) {
        itemView.tvNomePrato.text = cardapio.nomePrato
        itemView.tvIngredientes.text = cardapio.ingredientes
        itemView.tvPrecoPrato.text = cardapio.preco
    }
}