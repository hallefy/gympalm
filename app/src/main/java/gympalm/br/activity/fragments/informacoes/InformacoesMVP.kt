package gympalm.br.activity.fragments.informacoes

interface InformacoesMVP {
    interface View {
        fun request()
        fun setDataInfo()
    }

    interface Presenter {
        fun onViewReady()
        fun onViewGone()
    }

    interface Interactor {
        fun request()
        fun dispose()
    }
}