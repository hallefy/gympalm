package gympalm.br.activity.fragments.comentarios.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import gympalm.br.activity.listagem.adapter.ListagemAdapter
import gympalm.br.Models.ComentariosModel
import gympalm.br.R
import gympalm.br.utils.inflate

class ComentariosAdapter(val context: Context,
                         private val comentarios : List<ComentariosModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType){
            ListagemAdapter.ITEM -> {
                ComentariosViewHolder(context, parent.inflate(R.layout.card_comentatios))
            }
            else -> {
                ComentariosViewHolder(context, parent.inflate(R.layout.card_comentatios))
            }
        }
    }

    override fun getItemCount(): Int {
        return comentarios.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType){
            ListagemAdapter.ITEM -> {
                holder as ComentariosViewHolder
                holder.bind(comentarios[position])
            }
        }
    }

}