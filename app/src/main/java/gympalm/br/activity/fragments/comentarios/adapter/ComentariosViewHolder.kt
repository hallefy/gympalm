package gympalm.br.activity.fragments.comentarios.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import gympalm.br.Models.ComentariosModel
import kotlinx.android.synthetic.main.card_comentatios.view.*

class ComentariosViewHolder (val context: Context, itemView : View) : RecyclerView.ViewHolder(itemView){

    fun bind(comentariosModel: ComentariosModel) {
        itemView.tvUsuario.text = comentariosModel.usuario
        itemView.tvPrecoPrato.text = comentariosModel.tempo
        itemView.tvComentario.text = comentariosModel.comentario
    }
}