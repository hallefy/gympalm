package gympalm.br.activity.fragments.cardapio

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import gympalm.br.R
import gympalm.br.activity.fragments.cardapio.adapter.CardapioAdapter
import gympalm.br.utils.RandomData
import kotlinx.android.synthetic.main.fragment_cardapio.*

class CardapioFragment : Fragment() {
    lateinit var token : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val bundle = this.arguments
        if (bundle != null) {
            token = bundle.getString("token", "")
            Log.i("hallefy: ","token:$token")
        }

        return inflater.inflate(R.layout.fragment_cardapio, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler_cardapio.layoutManager = LinearLayoutManager(this.context)
        recycler_cardapio.adapter = CardapioAdapter(this.context!!, RandomData().cardapio())
    }
}
