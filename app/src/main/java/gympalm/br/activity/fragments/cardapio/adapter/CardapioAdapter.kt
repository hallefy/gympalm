package gympalm.br.activity.fragments.cardapio.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import gympalm.br.Models.CardapioModel
import gympalm.br.R
import gympalm.br.utils.inflate

class CardapioAdapter (val context: Context, private val estabelecimentos : List<CardapioModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType){
            ITEM -> {
                holder as CardapioViewHolder
                holder.bind(estabelecimentos[position])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType){
            ITEM -> {
                CardapioViewHolder(context, parent.inflate(R.layout.card_cardapio))
            }
            else -> {
                CardapioViewHolder(context, parent.inflate(R.layout.card_cardapio))
            }
        }
    }

    override fun getItemCount(): Int {
        return estabelecimentos.size
    }

    companion object {
        val ITEM = 0
    }
}