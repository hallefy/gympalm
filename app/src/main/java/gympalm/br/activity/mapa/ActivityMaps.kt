package  gympalm.br.activity.mapa

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import gympalm.br.R
import gympalm.br.utils.KEY
import gympalm.br.utils.Storage

class ActivityMaps : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    var storage = Storage()
    var latLngCurrentCity : LatLng? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_mapas)
        storage.init(this)

        val lat = storage.readFloat(KEY.LATITUDE)
        val long = storage.readFloat(KEY.LONGITUDE)

        latLngCurrentCity = LatLng(lat.toDouble(), long.toDouble())

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setMinZoomPreference(15.0f)
        mMap.setMaxZoomPreference(30.0f)

        val marker = MarkerOptions()
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_black))
        // Add a marker in Sydney and move the camera
        var burgerKing1 = LatLng(-16.697323, -49.256146)
        mMap.addMarker(marker.position(burgerKing1).title("Burger King"))

        val burgerKing2 = LatLng(-16.706761, -49.272787)
        mMap.addMarker(marker.position(burgerKing2).title("Burger King 1"))

        val burgerKing3 = LatLng(-16.702856, -49.272529)
        mMap.addMarker(marker.position(burgerKing3).title("Burger King 2"))

        val burgerKing4 = LatLng(-16.709104, -49.264161)
        mMap.addMarker(marker.position(burgerKing4).title("Burger King 3"))

        val burgerKing5 = LatLng(-16.694501, -49.265578)
        mMap.addMarker(marker.position(burgerKing5).title("Burger King 4"))

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLngCurrentCity))
    }
}
