package gympalm.br.activity.localizacao

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.android.gms.location.LocationRequest
import com.ninenine.reactivelocation.FusedLocationApiProvider
import com.ninenine.reactivelocation.LocationConnectionException
import com.ninenine.reactivelocation.LocationManager
import gympalm.br.R
import gympalm.br.utils.CustomProgressDialog
import gympalm.br.utils.KEY
import gympalm.br.utils.ShowAlertMessageSimple
import gympalm.br.utils.Storage
import rx.Subscription

class ActivityLocalizacao : AppCompatActivity() {

    val REQUEST_CODE_LOCATION_EXCEPTION = 1
    var subscription: Subscription? = null
    var hasLocation = false
    var storage = Storage()
    lateinit var progressDialog : CustomProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressDialog =  CustomProgressDialog(this).apply {
            setCancelable(true)
            setCanceledOnTouchOutside(true)
        }

        storage.init(this)

        showAlertInfo()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
        subscription?.unsubscribe()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_localizacao, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menuProximo -> {
                startActivityHome("default", "default", -0.0, 0.0)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun startActivityHome(cidade: String, estado: String, latitude : Double, longitude : Double) {
        progressDialog.dismiss()

         Log.i("hallefy: ","lat: $latitude - long: $longitude")

        if(cidade.isNotEmpty() || estado.isNotEmpty()){
            storage.write(KEY.KEY_CIDADE, cidade)
            storage.write(KEY.KEY_ESTADO, estado)
            storage.write(KEY.LATITUDE, latitude.toFloat())
            storage.write(KEY.LONGITUDE, longitude.toFloat())

        }else{
            ShowAlertMessageSimple(this).showAlert("Oops!",
                    "Cidade em branco, digite uma cidade e tente novamente.",
                    "Tentar novamente.")
        }
        this.startActivity(Intent(this,gympalm.br.activity.home.ActivityHome::class.java))
        this.finish()
    }

    private fun startActivityHome() {
        progressDialog.dismiss()
        this.startActivity(Intent(this,gympalm.br.activity.home.ActivityHome::class.java))
        this.finish()
    }

    private fun checkDataStorage() : Boolean{
        return (storage.read(KEY.KEY_CIDADE) != "" && storage.read(KEY.KEY_ESTADO) != "")
    }

    fun showAlertInfo() {
        if(storage.read(KEY.SHOW_ALERT_INFO_PERMISSION, true)){
            storage.write(KEY.SHOW_ALERT_INFO_PERMISSION, false)
            AlertDialog.Builder(this).apply {
                setTitle("Atenção")
                setMessage("Para obter sua cidade será necessário dar permissão para o App acessar sua localização.")
                setPositiveButton("ok", {_,_ ->
                    startListeningForLocations()
                })
                show()
            }
        }else {
            startListeningForLocations()
        }
    }

    private fun startListeningForLocations() {
        if(checkDataStorage()) {
            startActivityHome()
        } else{
            val locationApiProvider = FusedLocationApiProvider()
            val locationManager = LocationManager(locationApiProvider)
            val locationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(10000)
                    .setNumUpdates(1)
            subscription = locationManager.streamForRequest(this, locationRequest)
                    .subscribe(
                            { location ->
                                getLocationAdrdess(location)
                            },
                            { error ->
                                progressDialog.dismiss()

                                if (error is LocationConnectionException && error.hasSolution()) {
                                    error.startActivityForSolution(this, REQUEST_CODE_LOCATION_EXCEPTION)
                                }
                            }
                    )

        }
    }


    fun getLocationAdrdess(location : Location) {
        progressDialog.dismiss()

        val address = Geocoder(this).getFromLocation(location
                .latitude, location.longitude, 1)

        if(address[0].hasLatitude() && !hasLocation){
            hasLocation = true

            AlertDialog.Builder(this).apply {
                setTitle("Atenção")
                setMessage("Sua cidade atual é: ${address[0].subAdminArea} - ${address[0].adminArea}")
                setPositiveButton("ok", { _, _ ->
                    startActivityHome(address[0].subAdminArea, address[0].adminArea, address[0].latitude, address[0].longitude)
                })
                setNegativeButton("não", null)
                show()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_LOCATION_EXCEPTION && grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            progressDialog.show()
            startListeningForLocations()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_LOCATION_EXCEPTION && resultCode == Activity.RESULT_OK) {
            startListeningForLocations()
        }
    }
}
