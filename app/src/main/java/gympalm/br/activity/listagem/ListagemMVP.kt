package gympalm.br.activity.listagem

import android.content.Context
import gympalm.br.Models.Estabelecimentos
import io.reactivex.Observer

interface ListagemMVP {

    interface Presenter {
        fun addMoreEstabelecimentos()
        fun onViewReady()
        fun onViewGone()
    }

    interface Interactor {
        fun dispose()
        fun request(observer: Observer<List<Estabelecimentos>>)
    }

    interface View {
        fun requestList()
        fun showError()
        fun addMoreItem(estabelecimentos: List<Estabelecimentos>)
    }
}
