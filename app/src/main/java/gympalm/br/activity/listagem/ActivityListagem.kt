package gympalm.br.activity.listagem

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import gympalm.br.activity.listagem.adapter.ListagemAdapter
import gympalm.br.Models.Estabelecimentos
import gympalm.br.R
import kotlinx.android.synthetic.main.activity_listagem.*

class ActivityListagem : AppCompatActivity(), ListagemMVP.View {

    lateinit var presenter : ListagemPresenter
    var adapter : ListagemAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listagem)
        presenter = ListagemPresenter(this, this)

        createRecyclerView()
        requestList()
    }

    fun createRecyclerView() {
        recycler_home.layoutManager = LinearLayoutManager(this)
    }

    override fun requestList() {
        loadingItem(true)
        presenter.onViewReady()
    }

    override fun showError() {
    }

    fun loadingItem(isLoading : Boolean) {
        if (isLoading)
            progressBar.visibility = View.VISIBLE
        else
            progressBar.visibility = View.GONE
    }

    override fun addMoreItem(estabelecimentos : List<Estabelecimentos>) {
        loadingItem(false)
        if(adapter == null){
            adapter =  ListagemAdapter(this, estabelecimentos)
            recycler_home.adapter = adapter
        }else {
            adapter?.addEstabelecimentos(estabelecimentos)
        }
    }
}
