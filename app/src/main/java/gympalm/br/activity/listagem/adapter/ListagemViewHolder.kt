package gympalm.br.activity.listagem.adapter

import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import gympalm.br.Models.Estabelecimentos
import gympalm.br.R
import gympalm.br.activity.detalhes.ActivityDetalhes
import gympalm.br.utils.loadUrl
import kotlinx.android.synthetic.main.card_estabelecimento.view.*

class ListagemViewHolder(val context: Context, itemView : View) : RecyclerView.ViewHolder(itemView){
    @Suppress("DEPRECATION")
    fun bind(estabelecimentos: Estabelecimentos) {

        itemView.tvNome.text = estabelecimentos.nome_fantasia
        itemView.tvHorarioAtendimento.text = estabelecimentos.horario_funcionamento
        itemView.tvSetor.text = estabelecimentos.ramo
        itemView.ivImagem.loadUrl(estabelecimentos.logo)
        itemView.tvDistancia.text = estabelecimentos.distance

        if(estabelecimentos.is_open){
            itemView.tvStatus.setImageDrawable(context.resources.getDrawable(R.drawable.ic_open))
        }
        else{
            itemView.tvStatus.setImageDrawable(context.resources.getDrawable(R.drawable.ic_close))
        }

        itemView.card_view.setOnClickListener {
            goToDetails(estabelecimentos.token)
        }
    }

    fun goToDetails(token : String) {
        val intent = Intent(context, ActivityDetalhes::class.java)
        intent.putExtra("token", token)
         Log.i("hallefy","token: $token")
        context.startActivity(intent)
    }
}