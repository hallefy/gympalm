package gympalm.br.activity.listagem

import android.content.Context
import gympalm.br.Models.Estabelecimentos
import gympalm.br.network.NetworkApi
import gympalm.br.network.NetworkService
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit

class ListagemInteractor(var context: Context) : ListagemMVP.Interactor {
    var sub : Disposable? = null

    override fun request(observer : Observer<List<Estabelecimentos>>) {
        val retrofit : Retrofit = NetworkService().getClient(context)

        sub = retrofit.create(NetworkApi::class.java).getEstabelecimentos("11", "11")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({response -> observer.onNext(response)})
    }

    override fun dispose() {
        sub?.dispose()
    }
}