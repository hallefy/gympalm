package gympalm.br.activity.listagem

import android.content.Context
import android.util.Log
import gympalm.br.Models.Estabelecimentos
import io.reactivex.Observer
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import retrofit2.Call
import javax.inject.Inject

class ListagemPresenter @Inject constructor(
        var context: Context,
        val view : ListagemMVP.View):
        ListagemMVP.Presenter,
        Observer<List<Estabelecimentos>> {

    var interactor : ListagemInteractor = ListagemInteractor(context)

    override fun onComplete() {
    }

    override fun onSubscribe(d: Disposable?) {
    }

    override fun onNext(response: List<Estabelecimentos>) {
        view.addMoreItem(response)
    }

    override fun onError(e: Throwable?) {
    }

    override fun addMoreEstabelecimentos() {
    }

    override fun onViewReady() {
        interactor.request(this)
    }

    override fun onViewGone() {
        interactor.dispose()
    }
}