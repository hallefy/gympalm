package gympalm.br.activity.listagem.adapter
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.ViewGroup
import gympalm.br.Models.Estabelecimentos
import gympalm.br.R
import gympalm.br.utils.inflate

class ListagemAdapter(val context: Context, private val estabelecimentos : List<Estabelecimentos>) : Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType){
            ITEM -> {
                holder as ListagemViewHolder
                holder.bind(estabelecimentos[position])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType){
            ITEM -> {
                ListagemViewHolder(context, parent.inflate(R.layout.card_estabelecimento))
            }
            else -> {
                ListagemViewHolder(context, parent.inflate(R.layout.card_estabelecimento))
            }
        }
    }

    fun addEstabelecimentos(estabelecimentos : List<Estabelecimentos>) {

    }

    override fun getItemCount(): Int {
        return estabelecimentos.size
    }

    companion object {
        val ITEM = 0
    }
}