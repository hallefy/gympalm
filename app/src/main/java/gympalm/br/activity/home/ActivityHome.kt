package gympalm.br.activity.home

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import gympalm.br.R
import gympalm.br.activity.listagem.ActivityListagem
import gympalm.br.utils.KEY
import gympalm.br.utils.Storage
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_activity_home.*
import kotlinx.android.synthetic.main.nav_header_activity_home.view.*
import android.view.LayoutInflater
import gympalm.br.activity.cadastroEmpresa.ActivityCadastroEmpresa
import gympalm.br.activity.utilidadePublica.ActivityUtilidadePublica
import com.google.android.gms.maps.model.BitmapDescriptorFactory




class ActivityHome : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    var storage = Storage()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)
        storage.init(this)


        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        val header = navigationView.getHeaderView(0)
        val textMenuDrawer = header.findViewById<TextView>(R.id.tvCidadeEstadoDrawer)

        setCidadeEstadoMenuDrawer(textMenuDrawer)
    }

    fun setCidadeEstadoMenuDrawer(textMenu : TextView) {

        val cidade = storage.read(KEY.KEY_CIDADE)
        val estado = storage.read(KEY.KEY_ESTADO)

        if (estado!!.isNotEmpty() || cidade!!.isNotEmpty()){
            textMenu.text = "$cidade - $estado"
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setMinZoomPreference(15.0f)
        mMap.setMaxZoomPreference(30.0f)

        val marker = MarkerOptions()
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_black))

        var burgerKing1 = LatLng(-16.697323, -49.256146)
        mMap.addMarker(marker.position(burgerKing1)
                .title("Burger King"))


        val burgerKing2 = LatLng(-16.706761, -49.272787)
        mMap.addMarker(marker.position(burgerKing2).title("Burger King 1"))


        val burgerKing3 = LatLng(-16.702856, -49.272529)
        mMap.addMarker(marker.position(burgerKing3).title("Burger King 2"))



        val burgerKing4 = LatLng(-16.709104, -49.264161)
        mMap.addMarker(marker.position(burgerKing4).title("Burger King 3"))



        val burgerKing5 = LatLng(-16.694501, -49.265578)
        mMap.addMarker(marker.position(burgerKing5).title("Burger King 4"))

        mMap.moveCamera(CameraUpdateFactory.newLatLng(burgerKing5))
    }


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_listagem -> {
                startActivity(Intent(this.applicationContext, ActivityListagem::class.java))
            }
            R.id.nav_utilidade -> {
                startActivity(Intent(this.applicationContext, ActivityUtilidadePublica::class.java))
            }
            R.id.nav_cadastrarEmpresa -> {
                startActivity(Intent(this.applicationContext, ActivityCadastroEmpresa::class.java))
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


}
