package gympalm.br.activity.cadastroEmpresa

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import gympalm.br.R
import gympalm.br.activity.home.ActivityHome
import kotlinx.android.synthetic.main.activity_cadastro_empresa.*

class ActivityCadastroEmpresa : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastro_empresa)

        btnSubmeter.setOnClickListener {
            AlertDialog.Builder(this).apply {
                setTitle("Atenção!")
                setMessage("O seu cadastro foi submetido com sucesso. Entraremos em contato para tirar " +
                        "possíveis duvidas e completar o seu cadastro. Obrigado!")
                setPositiveButton("ok", {_, _ ->
                    startActivity(Intent(context, ActivityHome::class.java))
                })
                show()
            }
        }

    }
}
