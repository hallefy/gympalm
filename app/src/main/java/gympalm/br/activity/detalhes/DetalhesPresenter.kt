package gympalm.br.activity.detalhes

import android.content.Context
import gympalm.br.Models.EstabelecimentosDetalhe
import gympalm.br.activity.listagem.ListagemInteractor
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class DetalhesPresenter @Inject constructor(
        var context: Context,
        val view : DetalhesMVP.View) :
        DetalhesMVP.Presenter,
        Observer<EstabelecimentosDetalhe> {

    var interactor : DetalhesInteractor = DetalhesInteractor(context)

    override fun onComplete() {
    }

    override fun onSubscribe(d: Disposable?) {
    }

    override fun onNext(response: EstabelecimentosDetalhe?) {
        if(response != null){
            view.setDataInfo(response)
        }else {
            view.showError()
        }
    }

    override fun onError(e: Throwable?) {
        view.showError()
    }

    override fun onViewReady(token : String) {
        interactor.request(token, this)
    }

    override fun onViewGone() {
        interactor.dispose()
    }
}