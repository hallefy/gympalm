package gympalm.br.activity.detalhes

import gympalm.br.Models.EstabelecimentosDetalhe
import io.reactivex.Observer

interface DetalhesMVP {
    interface View {
        fun loadingItem(isLoading : Boolean)
        fun showError()
        fun request()
        fun setDataInfo(response: EstabelecimentosDetalhe)
    }

    interface Presenter {
        fun onViewReady(token : String)
        fun onViewGone()
    }

    interface Interactor {
        fun request(token : String,observer : Observer<EstabelecimentosDetalhe>)
        fun dispose()
    }
}