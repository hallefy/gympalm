package gympalm.br.activity.detalhes
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import gympalm.br.Models.EstabelecimentosDetalhe
import gympalm.br.R
import gympalm.br.activity.fragments.cardapio.CardapioFragment
import gympalm.br.activity.fragments.comentarios.ComentarioFragment
import gympalm.br.activity.fragments.informacoes.InformacoesFragment
import gympalm.br.activity.mapa.ActivityMaps
import gympalm.br.utils.CustomAdapter
import gympalm.br.utils.gone
import gympalm.br.utils.loadUrl
import gympalm.br.utils.visible
import kotlinx.android.synthetic.main.activity_detalhes.*


class ActivityDetalhes : AppCompatActivity(), DetalhesMVP.View{

    private var pagerAdapter : CustomAdapter? = null
    lateinit var presenter : DetalhesPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhes)

        presenter = DetalhesPresenter(this, this)

        val intent = this.intent.extras
        val token = intent.getString("token")

        pagerAdapter = CustomAdapter(supportFragmentManager)
        val bundle = Bundle()
        bundle.putString("token", token)
        InformacoesFragment().arguments

        pagerAdapter!!.addFragments(InformacoesFragment(), "Informações")
        pagerAdapter!!.addFragments(CardapioFragment(), "Cardápio")
        pagerAdapter!!.addFragments(ComentarioFragment(), "Comentários")

        container.adapter = pagerAdapter
        costomTabLayout.setupWithViewPager(container)

        fbMapas.setOnClickListener {
            startActivity(Intent(this, ActivityMaps::class.java))
        }

        presenter.onViewReady(token)
    }

    override fun loadingItem(isLoading: Boolean) {

    }

    override fun showError() {
    }

    override fun request() {
    }

    override fun setDataInfo(response: EstabelecimentosDetalhe) {
        tvTitulo.text = response.nome_fantasia
        tvRatingNota.text = response.nota_empresa
        ivEstabelecimento.loadUrl(response.logo)
        setImagemOpen(response.is_open)
        setCreditCard(response.passa_cartao)
    }

    private fun setCreditCard(passa_cartao: String) {
        if(passa_cartao.equals("1")){
            ivCartoes.visible()
        }else {
            ivCartoes.gone()
        }
    }

    private fun setImagemOpen(isOpen : Boolean) {
        if(isOpen){
            ivStatusDetalhes.setImageDrawable(resources.getDrawable(R.drawable.ic_open))
        } else {
            ivStatusDetalhes.setImageDrawable(resources.getDrawable(R.drawable.ic_close))
        }
    }
}
