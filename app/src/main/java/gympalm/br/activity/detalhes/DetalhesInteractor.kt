package gympalm.br.activity.detalhes

import android.content.Context
import gympalm.br.Models.Estabelecimentos
import gympalm.br.Models.EstabelecimentosDetalhe
import gympalm.br.network.NetworkApi
import gympalm.br.network.NetworkService
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit

class DetalhesInteractor (var context: Context) : DetalhesMVP.Interactor{

    var sub : Disposable? = null

    override fun request(token : String, observer : Observer<EstabelecimentosDetalhe>) {
        val retrofit : Retrofit = NetworkService().getClient(context)

        sub = retrofit.create(NetworkApi::class.java).getDetalhe(token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({response -> observer.onNext(response)})
    }

    override fun dispose() {
        sub?.dispose()
    }
}