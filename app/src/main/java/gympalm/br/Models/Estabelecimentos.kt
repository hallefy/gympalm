package gympalm.br.Models

data class Estabelecimentos(
    val token: String,
    val logo: String,
    val nome_fantasia: String,
    val is_open: Boolean,
    val distance: String,
    val ramo: String,
    val passa_cartao: String,
    val faz_entrega: String,
    val horario_funcionamento: String
)


data class EstabelecimentosDetalhe(
    val token: String,
    val cnpj: String,
    val data_hora: String,
    val setor: String,
    val logo: String,
    val horario_abertura: String,
    val horario_fechamento: String,
    val nome_fantasia: String,
    val latitude: String,
    val longitude: String,
    val is_open: Boolean,
    val razao_social: String,
    val ramo: String,
    val fone_comercial: String,
    val fone_whatsapp: String,
    val fone_alternativo: String,
    val nota_empresa: String,
    val passa_cartao: String,
    val descricao: String,
    val faz_entrega: String
)