package gympalm.br.Models

data class CardapioModel(
        var nomePrato : String,
        var ingredientes : String,
        var preco : String
)